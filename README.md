# MK eLottery

MK eLottery app

## Development

This app is developed using Flutter SDK (Hybrid mobile app developement framework from Google). You can learn about it on www.flutter.io

## Project structure

The main project files are:
- `pubspec.yaml` for configuration and adding dependencies/plugins
- `lib/` is where all code files are kept.
-- `lib/main.dart` is where the main function lives

## App download link

Here is the download link. http://bit.ly/mk-elottery

## How to run

I hope you have an android phone to download the app. The app works on both android and ios but releasing ios version is complex and expensive (like all apple products)

## Testing

I have added 100 test items in a private online database which we can access and update easily.

## Test cases

### Test case 1

You can enter any number from 1 to 100 and try your chance.

### Test case 2

Enter the same number again and it should say the item was given to someone else

### Test case 3

Enter a number that is not in the list (any number above 100) and it should show error message

## Todo

- show message when the lottery is over.
- display counter for how many are givenOut out of total (e.g. 20/100 given out)

## Lottery Options

ውዳሴ ማርያም  
ሐመር ተዋህዶ ቁ3  
ታአይኖ ቅዱሳን  
ቅዱሳን ስዕላት  
የታዙር ልጆች  
ወንድማማቾች  
እናት ቅድስት ቤ/ክ  
የፍቅር ዘለላ  
መርሐ ብዙሓን  
ሁለቱ ኪዳናት  
መንፈሳዊ ሕይወት ምንድነው?  
ወጣቶችና ፈተናዎቻቸው  
በዓላት  
ቃለ ምዕዳን  
ህያው ስም  
ከክፉ ዘመን ተጠበቁ  
አበው ምን ይላሉ?  
ሕይወተ ወራዙት  
ቅዱሳን ስዕላት የኪስ  
የአንገት ማተብ  
ሐመር መጽሔት 18ኛ አመት ቁ. 6  
ሐመር መጽሔት
