import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:validators/validators.dart';
import 'dialog_box.dart';
import 'dart:math';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MK eLottery',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() {
    return _MyHomePageState();
  }
}

class _MyHomePageState extends State<MyHomePage> {
  //final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final _formKey = GlobalKey<FormState>();

  BuildContext _context;

  int search;

  @override
  void initState() {
    super.initState();

    //generateGifts();

    StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance.collection('mk_elottery').snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) return LinearProgressIndicator();

        print("here");

        //return _buildList(context, snapshot.data.documents);
      },
    );
  }

  void generateGifts() {
    var list = [
      'ውዳሴ ማርያም',
      'ሐመር ተዋህዶ ቁ3',
      'ታአይኖ ቅዱሳን',
      'ቅዱሳን ስዕላት',
      'የታዙር ልጆች',
      'ወንድማማቾች',
      'እናት ቅድስት ቤ/ክ',
      'የፍቅር ዘለላ',
      'መርሐ ብዙሓን',
      'ሁለቱ ኪዳናት',
      'መንፈሳዊ ሕይወት ምንድነው?',
      'ወጣቶችና ፈተናዎቻቸው',
      'በዓላት',
      'ቃለ ምዕዳን',
      'ህያው ስም',
      'ከክፉ ዘመን ተጠበቁ',
      'አበው ምን ይላሉ?',
      'ሕይወተ ወራዙት',
      'ቅዱሳን ስዕላት የኪስ',
      'የአንገት ማተብ',
      'ሐመር መጽሔት 18ኛ አመት ቁ. 6',
      'ሐመር መጽሔት'
    ];

    final _random = new Random();

    /*
     * Generates a positive random integer uniformly distributed on the range
     * from [min], inclusive, to [max], exclusive.
     */
    int min = 1;
    int max = list.length;
    int next() => min + _random.nextInt(list.length - min);
    //int next(int min, int max) => min + _random.nextInt(max - min);

    // create new entry in firebase (100 elements)
    for (var i = 1; i <= 100; i++) {
      var nxt = next();
      print(nxt);

      Firestore.instance
          .collection('mk_elottery')
          .document()
          .setData({'id': i, 'gift': list[nxt], 'isGivenOut': false});
    }
  }

  String _validateSearchValue(String value) {
    print("value to validate " + value.length.toString());

    if (value != null && value.isNotEmpty && value.length > 0) {
      try {
        isNumeric(value);
      } catch (e) {
        return 'ያስገቡት ጽሑፍ ቁጥር አይደለም። እባክዎ ቁጥር ያስገቡ!';
      }
    } else {
      return 'ምንም ቁጥር አላስገቡም። እባክዎ ቁጥር ያስገቡ!';
    }
    return null;
  }

  bool submitting = false;

  void toggleSubmitState() {
    setState(() {
      submitting = !submitting;
    });
  }

  void submit() async {
    // First validate form.
    if (this._formKey.currentState.validate()) {
      toggleSubmitState();

      _formKey.currentState.save(); // Save our form now.

      Stream<QuerySnapshot> querySnapshots = Firestore.instance
          .collection('mk_elottery')
          .where("id", isEqualTo: search)
          .snapshots();
      //.then((data) => record = Record.fromSnapshot(data.documents.first));

      print('qss ' + querySnapshots.toString());

      if (querySnapshots.first == null || await querySnapshots.length == 0)
        print('querySnapshots is 0');

//QuerySnapshot querySnapshot =
      await querySnapshots.first.then((data) {
        DocumentSnapshot first = data.documents.first;

        //DocumentSnapshot first = querySnapshot.documents.first;

        if (first == null) print('first is null');

        Record record = Record.fromSnapshot(first);

        print(record.toString());

        toggleSubmitState();

        if (record != null) {
          if (!record.isGivenOut) {
            // show dialog

            const String _alertTitle = 'እንኳን ደስ አለዎት!';
            String _alertText = 'የ' + '${record.gift}' + ' ስጦታ ባለዕድል ሆነዋል!';

            DialogBox.showMessageDialog(_context, _alertTitle, _alertText);

            first.reference.updateData({'isGivenOut': true});

            // mark as givenOut
            Firestore.instance.runTransaction((Transaction tx) async {
              DocumentSnapshot postSnapshot = await tx.get(record.reference);

              Record newRecord = Record.fromSnapshot(postSnapshot);
              print(newRecord.toString());

              print('postSnapshot.reference.documentID ' +
                  postSnapshot.reference.documentID);
              if (postSnapshot.exists) {
                await tx.update(postSnapshot.reference,
                    <String, dynamic>{'gift': postSnapshot.data['gift'] + 1});
                //<String, dynamic>{'isGivenOut': true});
              }
            });

            // create new entry
            // Firestore.instance.collection('mk_elottery').document().setData(
            //     {'id': record.id, 'gift': record.gift, 'isGivenOut': true});
          } else {
            const String _alertTitle = 'ለሌላ እድለኛ ተሰጥቷል';
            String _alertText =
                'ያስገቡት ቁጥር ' + '$search' + ' ለሌላ እድለኛ ተሰጥቷል። እንደገና ይሞክሩ';

            DialogBox.showErrorDialog(context, _alertTitle, _alertText);
          }
        } else {
          const String _alertTitle = 'አልተሳካም';
          String _alertText = 'ያስገቡት ቁጥር ' + '$search' + ' አልተገኘም። እንደገና ይሞክሩ';

          DialogBox.showErrorDialog(context, _alertTitle, _alertText);
        }
      }).catchError((onError) {
        toggleSubmitState();

        const String _alertTitle = 'አልተሳካም';
        String _alertText = 'ያስገቡት ቁጥር ' + '$search' + ' አልተገኘም። እንደገና ይሞክሩ';

        DialogBox.showErrorDialog(context, _alertTitle, _alertText);

        return;
      });

      //DocumentSnapshot first = querySnapshot.documents.first;

      // if (first == null) print('first is null');

      // Record record = Record.fromSnapshot(first);

      // print(record.toString());

      // toggleSubmitState();

      // if (record != null) {
      //   if (!record.isGivenOut) {
      //     // show dialog

      //     const String _alertTitle = 'እንኳን ደስ አለዎት!';
      //     String _alertText = 'የ' + '${record.gift}' + ' ስጦታ ባለዕድል ሆነዋል!';

      //     DialogBox.showMessageDialog(_context, _alertTitle, _alertText);

      //     // mark as givenOut
      //   } else {
      //     const String _alertTitle = 'ለሌላ እድለኛ ተሰጥቷል';
      //     String _alertText =
      //         'ያስገቡት ቁጥር ' + '$search' + ' ለሌላ እድለኛ ተሰጥቷል። እንደገና ይሞክሩ';

      //     DialogBox.showErrorDialog(context, _alertTitle, _alertText);
      //   }
      // } else {
      //   const String _alertTitle = 'አልተሳካም';
      //   String _alertText = 'ያስገቡት ቁጥር ' + '$search' + ' አልተገኘም። እንደገና ይሞክሩ';

      //   DialogBox.showErrorDialog(context, _alertTitle, _alertText);
      // }

      // .then((data) =>
      //     data.documents.first["id"].toString() +
      //     " " +
      //     data.documents.first["gift"] +
      //     " " +
      //     data.documents.first["isGivenOut"].toString());

      //print('found ' + first.toString());
      // .listen((data) => data.documents.forEach((doc) => print(
      //     doc["id"].toString() +
      //         " " +
      //         doc["gift"] +
      //         " " +
      //         doc["isGivenOut"].toString())));

      // .map(snapshot) {
      //     print('snapshot.hasData ' + snapshot.hasData);
      //     if (!snapshot.hasData) print('no data yet - loading ...');

      //     snapshot.data.documents.map((data) {
      //       final record = Record.fromSnapshot(data);
      //       print(record.id.toString() + " ^ " + record.gift);
      //     });
      // };

      //_buildBody(_context);
      //await _fireSearch(search);
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: Center(
          child: new Text('ኢ-ሎተሪ : ማኅበረ ቅዱሳን'),
        ),
      ),
      body: new Builder(builder: (BuildContext context) {
        _context = context;
        return _buildFormBody(context);
        //return _buildBody(context);
      }),
    );
  }

  Widget _buildFormBody(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;

    return submitting
        ? const Center(child: const CircularProgressIndicator())
        : new Container(
            padding: new EdgeInsets.all(20.0),
            child: new Form(
              key: _formKey,
              child: new ListView(
                children: <Widget>[
                  new TextFormField(
                      keyboardType: TextInputType.number,
                      // Use number input type.
                      //initialValue: '3',
                      decoration:
                          new InputDecoration(labelText: 'እድለኛ ቁጥርዎን ያስገቡ'),
                      validator: this._validateSearchValue,
                      onSaved: (String value) {
                        this.search = toInt(value);
                      }),
                  new Container(
                    width: screenSize.width,
                    child: new RaisedButton(
                      child: new Text(
                        'ይሞክሩ',
                        style: new TextStyle(color: Colors.white),
                      ),
                      onPressed: this.submit,
                      color: Colors.blue,
                    ),
                    margin: new EdgeInsets.only(top: 20.0),
                  )
                ],
              ),
            ),
          );
  }

  Widget _buildBody(BuildContext context) {
    print('_buildBody: Printing the search data.');
    print('_buildBody: Search Value: ${search.toString()}');

    return StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance.collection('mk_elottery').snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) return LinearProgressIndicator();

        return _buildList(context, snapshot.data.documents);
      },
    );
  }

  Future _fireSearch(int search) async {
    print('Printing the search data.');
    print('Search Value: ${search.toString()}');

    return StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance.collection('mk_elottery').snapshots(),
        builder: (context, snapshot) {
          print(snapshot.hasData);
          if (!snapshot.hasData) return LinearProgressIndicator();

          snapshot.data.documents.map((data) {
            final record = Record.fromSnapshot(data);
            print(record.id.toString() + " ^ " + record.gift);
          });

          toggleSubmitState();
        });
    //print(collectionReference.where("id", isEqualTo:search).snapshots().first);
  }

  Widget _buildList(BuildContext context, List<DocumentSnapshot> snapshot) {
    return ListView(
      padding: const EdgeInsets.only(top: 20.0),
      children: snapshot.map((data) => _buildListItem(context, data)).toList(),
    );
  }

  Widget _buildListItem(BuildContext context, DocumentSnapshot data) {
    final record = Record.fromSnapshot(data);

    return Padding(
      key: ValueKey(record.id),
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.grey),
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: ListTile(
          leading: Text(record.id.toString()),
          title: Text(record.gift),
          trailing: Text(record.isGivenOut.toString()),
          onTap: () => Firestore.instance.runTransaction((transaction) async {
            final freshSnapshot = await transaction.get(record.reference);
            final fresh = Record.fromSnapshot(freshSnapshot);

            //print(fresh.gift + " " + fresh.id.toString() + " " + fresh.isGivenOut.toString());

            await transaction
                .update(record.reference, {'isGivenOut': !fresh.isGivenOut});
          }),
        ),
      ),
    );
  }
}

class Record {
  final int id;
  final String gift;
  final bool isGivenOut;
  final DocumentReference reference;

  Record.fromMap(Map<String, dynamic> map, {this.reference})
      : assert(map['id'] != null),
        assert(map['gift'] != null),
        assert(map['isGivenOut'] != null),
        id = map['id'],
        gift = map['gift'],
        isGivenOut = map['isGivenOut'];

  Record.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data, reference: snapshot.reference);

  @override
  String toString() => "Record<$id:$gift:$isGivenOut:${reference.documentID}>";
}
