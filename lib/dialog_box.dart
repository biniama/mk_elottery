import 'dart:async';
import 'package:flutter/material.dart';

class DialogBox {

  static Future<DialogAction> showMessageDialog(BuildContext context, String title, String content) {

    final ThemeData theme = Theme.of(context);
    final TextStyle dialogTextStyle =
      theme.textTheme.subhead.copyWith(color: theme.textTheme.caption.color);

    return showDialog<DialogAction>(
      context: context,
      builder: (BuildContext context) => new AlertDialog(
          title: Text(title),
          content: Text(content, style: dialogTextStyle),
          actions: <Widget>[
            new FlatButton(
                child: const Text('OK'),
                onPressed: () {
                  Navigator.pop(context, DialogAction.ok);
                })
          ]),
    );
  }

  static Future<DialogAction> showScrollableMessageDialog(BuildContext context, String title, String content) {

    final ThemeData theme = Theme.of(context);
    final TextStyle dialogTextStyle =
    theme.textTheme.subhead.copyWith(color: theme.textTheme.caption.color);

    return showDialog<DialogAction>(
      context: context,
      builder: (BuildContext context) => new AlertDialog(
          title: Text(title),
          content:
          SingleChildScrollView(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [Text(content, style: dialogTextStyle),],
            ),
          ),
          actions: <Widget>[
            new FlatButton(
                child: const Text('OK'),
                onPressed: () {
                  Navigator.pop(context, DialogAction.ok);
                })
          ]),
    );
  }

  //todo not used because shows snackbar at last
  static void showErrorDialog(BuildContext context, String alertTitle, String alertText) {

    final ThemeData theme = Theme.of(context);
    final TextStyle dialogTextStyle =
    theme.textTheme.subhead.copyWith(color: theme.textTheme.caption.color);

    showDialog<DialogAction>(
      context: context,
      builder: (BuildContext context) => new AlertDialog(
          title: Text(alertTitle),
          content: Text(alertText, style: dialogTextStyle),
          actions: <Widget>[
            new FlatButton(
                child: const Text('OK'),
                onPressed: () {
                  Navigator.pop(context, DialogAction.ok);
                })
          ]),
    ).then<void>((DialogAction value) {
      // The value passed to Navigator.pop() or null.
      if (value != null) {
        _showSnackBar(context);
      }
    });
  }

  static void _showSnackBar(BuildContext _scaffoldContext) {
    Scaffold.of(_scaffoldContext).showSnackBar(new SnackBar(
      content: new Text('This is snackbar.'),
      action: new SnackBarAction(
          label: 'ACTION',
          onPressed: () {
            Scaffold.of(_scaffoldContext).showSnackBar(new SnackBar(
                content: new Text('You pressed snackbar\'s action.')));
          }),
    ));
  }

  //unused because don't know where it goes after clicking
  static Future<void> showAlertDialog({BuildContext context, String title, String description}) =>
      showDialog<void>(
          context: context,
          barrierDismissible: true,
          builder: (context) => AlertDialog(
            title: Text(title),
            content: SingleChildScrollView(
              child: Text(description),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          ));
}

enum DialogAction {
  ok,
  cancel,
  discard,
  disagree,
  agree,
}